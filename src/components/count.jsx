import { Component } from "react";

class Count extends Component {
    constructor (props){
        super(props)
        this.state = {
            count: 1
        }
    }

    addCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    componentWillMount(){
        console.log('Component Will Mount')
    };

    componentDidMount() {
        console.log('Component Did Mount')
        setInterval(this.addCount, 1000)
    }

    shouldComponentUpdate(){
        console.log('Should Component Update')
        return true
    }

    componentWillUpdate(){
        console.log('Component Will Update')
    }

    componentDidUpdate(){
        console.log('Component Did Update')
        if(this.state.count === 10){
            this.props.dislayProps()
        }
    }

    componentWillUnmount(){
        console.log('Component Will Unmount')
        
    }
    render (){
        console.log('render count',this.state.count )
        return (
            <>
            <p>Count: {this.state.count}</p>
            </>
        )
    }
}

export default Count