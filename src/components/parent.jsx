import { Component } from "react";
import Count from "./count";

class Parent extends Component {
    constructor(props){
        super(props)
        this.state = {
            dislay: true
        }
    }

    updateDispalay = () => {
        this.setState ({
            dislay: false
        })
    }

    render(){
        const {dislay} = this.state
        return (
            <div>
               { dislay ? <Count dislayProps={this.updateDispalay}/> : null}
            </div>
        )
    }
}

export default Parent